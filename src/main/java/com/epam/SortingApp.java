package com.epam;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

/**
 * @author Kanybek Mukalaev
 */
public class SortingApp {
    public static void main(String[] args) {
        SortingApp sortingApp = new SortingApp();
        File file = new File("src\\main\\resources\\file.txt");
        String [] str;
        if (args.length > 10){
            try {
                Scanner scanner = new Scanner(file);
                String [] arr = scanner.nextLine().split(" ");
                System.out.println(Arrays.toString(sortingApp.sortInputAndPrint(args)));
            } catch (FileNotFoundException e) {
                System.out.println("No file found.");
            }
        } else {
            System.out.println(Arrays.toString(sortingApp.sortInputAndPrint(args)));
        }
    }
    public String [] sortInputAndPrint(String [] args){
        if (args == null || args.length == 0){
            throw new IllegalArgumentException();
        }
        int [] array = new int[args.length];
        String [] arrayString = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            array[i] = Integer.parseInt(args[i]);
        }

        Arrays.sort(array);
        for (int i = 0; i < array.length; i++) {
            arrayString[i] = String.valueOf(array[i]);
        }
        return arrayString;
    }
}
