package com.epam;


import org.junit.Test;

public class SortingAppTest {
    SortingApp sortingApp = new SortingApp();


    @Test(expected = IllegalArgumentException.class)
    public void testWhenArgIsNull() throws Exception{
        String [] args = null;
        sortingApp.sortInputAndPrint(args);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testWhenArgIsEmpty() throws Exception{
        String [] args = {};
        sortingApp.sortInputAndPrint(args);
    }
    @Test(expected = NumberFormatException.class)
    public void testIfThereIsStringValueInArgs(){
        String [] args = {"1", "2", "8", "19", "epam"};
        sortingApp.sortInputAndPrint(args);
    }
    @Test(expected = NumberFormatException.class)
    public void testIfThereIsDecimalNumberInValueInArgs(){
        String [] args = {"1", "2", "8", "19", "1.0"};
        sortingApp.sortInputAndPrint(args);
    }

}